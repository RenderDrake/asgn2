﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using iab330asgn2.Droid;
using iab330asgn2.Models;
using Xamarin.Forms;

[assembly: Dependency(typeof(SaveAndLoad))]
namespace iab330asgn2.Droid
{
	public class SaveAndLoad : ISaveAndLoad
	{
    public string LoadText(string filename)
    {
        throw new NotImplementedException();
    }

    //Method to save a list of products into a text file
    public void SaveText(string filename, ObservableCollection<Product> p)
    {
            List<String> s = new List<string>();
            foreach(Product prod in p){
                s.Add(prod.formatString());  
            }
			var pathFile = Android.OS.Environment.GetExternalStoragePublicDirectory(Android.OS.Environment.DirectoryDownloads);
			var absolutePath = pathFile.AbsolutePath;
		    var filePath = Path.Combine(absolutePath, filename);

            //Writes the list to the tetx file and saves it to the downloads folder
            File.WriteAllLines(filePath, s.ToArray());
            if(File.Exists(filePath)){
                Application.Current.MainPage.DisplayAlert(filename, "Was created!", "OK");
            }

    }
}
}