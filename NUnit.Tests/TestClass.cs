﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iab330asgn2.ViewModels;
using iab330asgn2.Models;
using iab330asgn2;

namespace NUnit.Tests
{
	[TestFixture]
	public class TestClass
	{
		[Test]
		public void TestMethod()
		{
			// TODO: Add your test code here
			Assert.Pass("Your first passing test");
		}

        [TestFixture]
        public partial class TestGrocery
        {
            Grocery grocery;
            [SetUp]
            public void SetUp()
            {
                grocery = new Grocery { };
            }

            [Test]
            public void TestGroceryName()
            {
                String name = "cookies";
                grocery.DisplayName = name;
                Assert.AreEqual(grocery.DisplayName, name);
            }

            [Test]
            public void TestGroceryInvalidName()
            {
                String name = "";
                var ex = Assert.Throws<Exception>(() => grocery.DisplayName = name);
                Assert.That(ex, Is.EqualTo("Invalid name."));
            }

            [Test]
            public void TestGroceryAisleNumber()
            {
                int aislenum = 1;
                grocery.AisleNumber = aislenum;
                Assert.AreEqual(grocery.AisleNumber, aislenum);
            }

            [Test]
            public void TestGroceryInvalidAisleNumber()
            {
                int aislenum = 0;
                var ex = Assert.Throws<Exception>(() => grocery.AisleNumber = aislenum);
                Assert.That(ex, Is.EqualTo("Invalid aisle number."));
            }

            [Test]
            public void TestGroceryCost()
            {
                decimal cost = 1.05m;
                grocery.Cost = cost;
                Assert.AreEqual(grocery.Cost, cost);
            }

            [Test]
            public void TestGroceryInvalidCost()
            {
                decimal cost = 0;
                var ex = Assert.Throws<Exception>(() => grocery.Cost = cost);
                Assert.That(ex, Is.EqualTo("Invalid cost."));
            }

            [Test]
            public void TestGroceryAmount()
            {
                int amount = 1;
                grocery.Amount = amount;
                Assert.AreEqual(grocery.Amount, amount);
            }

            [Test]
            public void TestGroceryInvalidAmount()
            {
                int amount = 0;
                var ex = Assert.Throws<Exception>(() => grocery.Amount = amount);
                Assert.That(ex, Is.EqualTo("Invalid amount."));
            }

            [Test]
            public void TestGrocerySame()
            {
                String name = "cookies";
                int aislenum = 1;
                decimal cost = 1.05m;
                int amount = 1;
                Grocery grocery_two = new Grocery {
                    DisplayName = name,
                    AisleNumber = aislenum,
                    Cost = cost,
                    Amount = amount
                };
                grocery.DisplayName = name;
                grocery.AisleNumber = aislenum;
                grocery.Cost = cost;
                grocery.Amount = amount;
                Assert.AreEqual(grocery.DisplayName, grocery_two.DisplayName);
                Assert.AreEqual(grocery.AisleNumber, grocery_two.AisleNumber);
                Assert.AreEqual(grocery.Cost, grocery_two.Cost);
                Assert.AreEqual(grocery.Amount, grocery_two.Amount);
            }
        }
        
        [TestFixture]
        public partial class TestProduct
        {
            Product product;
            [SetUp]
            public void Setup()
            {
                product = new Product { };
            }
            [Test]
            public void TestProductName()
            {
                String name = "cookies";
                product.DisplayName = name;
                Assert.AreEqual(product.DisplayName, name);
            }
            [Test]
            public void TestProductInvalidName()
            {
                String name = "";
                var ex = Assert.Throws<Exception>(() => product.DisplayName = name);
                Assert.That(ex, Is.EqualTo("Invalid name."));
            }
            [Test]
            public void TestProductAisleNumber()
            {
                int aislenum = 1;
                product.AisleNumber = aislenum;
                Assert.AreEqual(product.AisleNumber, aislenum);
            }
            [Test]
            public void TestProductInvalidAisle()
            {
                int aislenum = 0;
                var ex = Assert.Throws<Exception>(() => product.AisleNumber = aislenum);
                Assert.That(ex, Is.EqualTo("Invalid aisle number."));
            }
            [Test]
            public void TestProductCost()
            {
                decimal cost = 1.05m;
                product.Cost = cost;
                Assert.AreEqual(product.Cost, cost);
            }
            [Test]
            public void TestProductInvalidCost()
            {
                decimal cost = 0;
                var ex = Assert.Throws<Exception>(() => product.Cost = cost);
                Assert.That(ex, Is.EqualTo("Invalid cost."));
            }
            [Test]
            public void TestProductType()
            {
                String type = "food";
                product.Type = type;
                Assert.AreEqual(product.Type, type);
            }

            [Test]
            public void TestProductInvalidType()
            {
                String type = "";
                var ex = Assert.Throws<Exception>(() => product.Type = type);
                Assert.That(ex, Is.EqualTo("Invalid type."));
            }

            [Test]
            public void TestProductSame()
            {
                String name = "cookies";
                int aislenum = 1;
                decimal cost = 1.05m;
                String type = "food";
                product.DisplayName = name;
                product.AisleNumber = aislenum;
                product.Cost = cost;
                product.Type = type;
                Product product_two = new Product
                {
                    DisplayName = name,
                    AisleNumber = aislenum,
                    Cost = cost,
                    Type = type
                };
                Assert.AreEqual(product.DisplayName, product_two.DisplayName);
                Assert.AreEqual(product.AisleNumber, product_two.AisleNumber);
                Assert.AreEqual(product.Cost, product_two.Cost);
                Assert.AreEqual(product.Type, product_two.Type);
            }
        }

        [Test]
        public void TestDeal()
        {
            String url = "http://localhost/";
            Deal deal = new Deal
            {
                URL = url
            };
            Assert.AreEqual(deal.URL, url);
        }

    }
}
