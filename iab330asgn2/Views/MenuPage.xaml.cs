﻿using System;
using System.Collections.Generic;
using iab330asgn2.Views;
using Xamarin.Forms;

namespace iab330asgn2
{
	public partial class MenuPage : ContentPage
	{
		/// <summary>
        /// This class is used display menu page.
		/// It displays fields to navigate to the profile or add product pages.
        /// </summary>
		
		public MenuPage()
		{
			InitializeComponent();
		}


		async void Profile_Activated(object sender, System.EventArgs e)
		{
			await Navigation.PushAsync(new ProfilePage());

		}

		async void Add_Product(object sender, System.EventArgs e)
		{
			await Navigation.PushAsync(new AddProductPage());
		}
	}
}
