﻿using System;
using System.Collections.Generic;
using iab330asgn2.Models;
using iab330asgn2.ViewModels;
using Xamarin.Forms;

namespace iab330asgn2.Views
{
    public partial class EditProductPage : ContentPage
    {
        /// <summary>
        /// This class is used display the fields neccessary for editing a product.
        /// It is bound to the EditProductViewModel class
        /// </summary>
        
        private Product p;
        public EditProductPage(Product product)
        {
            p = product;
            InitializeComponent();
            BindingContext = new EditProductViewModel(p);
        }

		async void Menu_Activated(object sender, System.EventArgs e)
		{
			await Navigation.PushAsync(new MenuPage());

		}

		async void Profile_Activated(object sender, System.EventArgs e)
		{
			await Navigation.PushAsync(new ProfilePage());

		}
    }
}
