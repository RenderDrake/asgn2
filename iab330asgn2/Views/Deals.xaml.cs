﻿using System;
using System.Collections.Generic;
using iab330asgn2.ViewModels;
using Xamarin.Forms;

namespace iab330asgn2.Views
{
	public partial class Deals : ContentPage
	{
		/// <summary>
        /// This class is used display the available deals.
        /// It is bound to the DealViewModel class
        /// </summary>
		
		public Deals()
		{
			InitializeComponent();
            BindingContext = new DealViewModel();
		}

		async void Menu_Activated(object sender, System.EventArgs e)
		{
			await Navigation.PushAsync(new MenuPage());

		}

		async void Profile_Activated(object sender, System.EventArgs e)
		{
			await Navigation.PushAsync(new ProfilePage());

		}
	}
}
