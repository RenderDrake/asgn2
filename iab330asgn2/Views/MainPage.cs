﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;


namespace iab330asgn2.Views
{
	public partial class MainPage : TabbedPage
	{
		/// <summary>
        /// This class is used setup the main page, cosnsiting of the content tabs
        /// </summary>
		
		public MainPage()
		{
			InitializeComponent();
		}
	}
}
