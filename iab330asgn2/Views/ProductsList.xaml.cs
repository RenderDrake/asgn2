﻿using System;
using System.Collections.Generic;
using iab330asgn2.ViewModels;
using Xamarin.Forms;
using iab330asgn2.Models;

namespace iab330asgn2.Views
{
	public partial class ProductsList : ContentPage
	{
       
		/// <summary>
        /// This class is used display the list of available products.
        /// It is bound to the ProductViewModel class
        /// </summary>


        public ProductsList()
		{
			InitializeComponent();
			BindingContext = new ProductViewModel();
		}

		async void Add_Product(object sender, System.EventArgs e)
		{
			await Navigation.PushAsync(new AddProductPage());
		}
		async void Menu_Activated(object sender, System.EventArgs e)
		{
            await Navigation.PushAsync(new MenuPage());

		}
		async void Profile_Activated(object sender, System.EventArgs e)
		{
			await Navigation.PushAsync(new ProfilePage());

        }


       
    }
}
