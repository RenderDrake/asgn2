﻿using System;
using System.Collections.Generic;
using iab330asgn2.ViewModels;

using Xamarin.Forms;

namespace iab330asgn2.Views
{
	public partial class MyList : ContentPage
	{
		/// <summary>
        /// This class is used display the users list of groceries.
		/// It is bound to the GroceryViewModel class
        /// </summary>
		
		public MyList()
		{
			InitializeComponent();
			BindingContext = new GroceryViewModel();
		}
		async void Menu_Activated(object sender, System.EventArgs e)
		{
			await Navigation.PushAsync(new MenuPage());

		}

		async void Profile_Activated(object sender, System.EventArgs e)
		{
			await Navigation.PushAsync(new ProfilePage());

		}
	}
}
