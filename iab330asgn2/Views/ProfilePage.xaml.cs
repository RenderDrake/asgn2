﻿using System;
using System.Collections.Generic;
using iab330asgn2.Views;
using Xamarin.Forms;

namespace iab330asgn2
{
	public partial class ProfilePage : ContentPage
	{
		/// <summary>
        /// This class is used display the information about the user
        /// </summary>
		
		public ProfilePage()
		{
			InitializeComponent();
		}

		async void Menu_Activated(object sender, System.EventArgs e)
		{
			await Navigation.PushAsync(new MenuPage());

		}

		async void Profile_Activated(object sender, System.EventArgs e)
		{
			await Navigation.PushAsync(new ProfilePage());

		}
	}
}
