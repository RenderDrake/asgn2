﻿using System;
using System.Collections.Generic;
using iab330asgn2.ViewModels;
using Xamarin.Forms;

namespace iab330asgn2.Views
{
    public partial class AddProductPage : ContentPage
    {
        /// <summary>
        /// This class is used display the fields neccessary for adding a new product.
        /// It is bound to the AddProductViewModel class
        /// </summary>
        
        public AddProductPage()
        {
            InitializeComponent();
            BindingContext = new AddProductViewModel();

        }
    }
}
