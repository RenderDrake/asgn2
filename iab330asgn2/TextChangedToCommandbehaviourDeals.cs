﻿using System;
using iab330asgn2.ViewModels;
using Xamarin.Forms;
namespace iab330asgn2
{
    public class TextChangedToCommandbehaviour: Behavior<SearchBar>
    {

        //Method to bind the text changed event to a command to reduce code in View
        protected override void OnAttachedTo(SearchBar bindable){
            base.OnAttachedTo(bindable);
            bindable.TextChanged += Bindable_TextChanged;
        }

        //Method to fire the search command when the text changes
        private void Bindable_TextChanged(object sender, TextChangedEventArgs e)
        {
            var sb = sender as SearchBar;
            var vm = sb.BindingContext as ProductViewModel;
            vm.Search();
        }
        //Method to un-bind the command when it is finished
        protected override void OnDetachingFrom(SearchBar bindable)
        {
            base.OnDetachingFrom(bindable);
            bindable.TextChanged -= Bindable_TextChanged;
        }
        public TextChangedToCommandbehaviour()
        {
        }
    }
}
