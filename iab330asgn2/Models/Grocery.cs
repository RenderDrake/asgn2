﻿using System;
using System.ComponentModel;
using iab330asgn2.Models.Database;

namespace iab330asgn2.Models
{
	//Defines the structure of a grocery item
	public class Grocery : IdBase, INotifyPropertyChanged
	{
		/// <summary>
        /// This class is used define what a grocery item consists of.
        /// </summary>
		
		private string displayName;
		private int aisleNumber;
        private decimal cost;
		private int amount;
		private int baseCost;
        private int id;
        public decimal BaseCost { get; set; }


       
		//Gets and Sets the grocery display name
		public String DisplayName
		{

			get
			{
				return displayName;
			}

			set
			{
				displayName = value;
				NotifyPropertyChanged("DisplayName");
			}

		}

		//Gets and Sets the grocery aisle number
		public int AisleNumber
		{

			get
			{
				return aisleNumber;
			}

			set
			{
				aisleNumber = value;
				NotifyPropertyChanged("AisleNumber");
			}

		}

		//Gets and Sets the grocery cost
		public decimal Cost
		{

			get
			{
				return cost;
			}

			set
			{
				cost = value;
				NotifyPropertyChanged("Cost");
			}

		}

		//Gets and Sets the amount of that grocery
		public int Amount
		{

			get
			{
				return amount;
			}

			set
			{
				amount = value;
				NotifyPropertyChanged("Amount");
			}

		}

		//Method to notify the GUI if a change in grocery
		public event PropertyChangedEventHandler PropertyChanged;
		private void NotifyPropertyChanged(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
	}
}





