﻿using System;
using System.ComponentModel;
using iab330asgn2.Models.Database;

namespace iab330asgn2.Models
{

	//Class to define a product item
	public class Product : IdBase, INotifyPropertyChanged
	{
		/// <summary>
        /// This class is used to define what a product item consists of and how they appear when exported
        /// </summary>
		
		private string displayName;
		private int aisleNumber;
        private decimal cost;
        private string type;


		//Gets and Sets the product type
        public string Type
        {
            get
            {
                return type;
            }
            set
            {
                type = value;
                NotifyPropertyChanged("Type");
            }


        }

		//Gets and Sets the product display name
		public string DisplayName
		{
			get
			{
				return displayName;
			}
			set
			{
				displayName = value;
				NotifyPropertyChanged("DisplayName");
			}


		}

		//Gets and Sets the product aisle number
		public int AisleNumber
		{

			get
			{
				return aisleNumber;
			}

			set
			{
				aisleNumber = value;
				NotifyPropertyChanged("AisleNumber");
			}

		}
		
		//Gets and Sets the product cost
        public decimal Cost
		{

			get
			{
				return cost;
			}

			set
			{
				cost = value;
				NotifyPropertyChanged("Cost");
			}

		}

		//Returns the formatted string of the products for exporting
        public string formatString()
        {
            return string.Format("[Product: DisplayName={0}, AisleNumber={1}, Cost={2}]", DisplayName, AisleNumber, Cost);
        }




		//Method to notify the GUI of a product update
		public event PropertyChangedEventHandler PropertyChanged;
		public void NotifyPropertyChanged(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}

        public static explicit operator Product(string v)
        {
            throw new NotImplementedException();
        }
    }

}


