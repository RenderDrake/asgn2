﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml.Serialization;
using iab330asgn2.Models.Database;

namespace iab330asgn2.Models
{
    public class populate_products
    {
        /// <summary>
        /// This class is used to initially populate the products database with default items
        /// </summary>
        
        //Populate the products databse with a text file containing default products
        public populate_products(Database.Database db)
        {
            var assembly = typeof(populate_products).GetTypeInfo().Assembly;
            Stream stream = assembly.GetManifestResourceStream("iab330asgn2.Models.Database.AUSNUT 2007 - Food Database.txt");
            string text = "";
            using (var reader = new System.IO.StreamReader(stream))
            {
                text = reader.ReadToEnd();
            }

            List<string> listStrLineElements = text.Split(new string[] { Environment.NewLine }, StringSplitOptions.None).ToList();// You need using System.Linq at the top.

            //Iterate through the text file items and add them to the products database
            foreach(String s in listStrLineElements){
                Random aisle = new Random();
                Random cost = new Random();
                db.SaveItem(new Product
                {
                    DisplayName = s,
                    AisleNumber = aisle.Next(1, 10),
                    Cost = (decimal)Math.Round((cost.NextDouble() * 7.8), 2)
                                        
                });
            }

        }
    }
}
