﻿using System;
using System.ComponentModel;

namespace iab330asgn2.Models
{

    //Defines the structure of a deal item
    public class Deal : Product, INotifyPropertyChanged
    {
        /// <summary>
        /// This class is used to define what a deal consists of.
        /// It inherits properties from the product model
        /// </summary>
        private string url;

        public string URL
        {
            get
            {
                return url;
            }
            set
            {
                url = value;
                NotifyPropertyChanged("URL");
            }

           

        }

    }
}
