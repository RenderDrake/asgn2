﻿using System;
using SQLite;
namespace iab330asgn2.Models.Database
{
    public class IdBase: IDatabaseId
    {
        public IdBase()
        {
        }

		[PrimaryKey, AutoIncrement]
		public int Id
		{
			get; set;
		}
    }
}
