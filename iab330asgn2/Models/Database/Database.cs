﻿using System;
using SQLite;
using System.Collections.Generic;
using System.Linq;
using System.Collections.ObjectModel;

namespace iab330asgn2.Models.Database
{
    public class Database
    {
		SQLiteConnection Connection { get; }
		public static string Root { get; set; } = string.Empty;

		public Database()
		{
			var location = "MyListDB.db3";
			location = System.IO.Path.Combine(Root, location);
			Connection = new SQLiteConnection(location);
            Connection.DropTable<Deal>();
            Connection.CreateTable<Product>();
            Connection.CreateTable<Grocery>();
            Connection.CreateTable<Deal>();

		}


        public List<T> GetItems<T>() where T : IDatabaseId, new()
		{

			return (from i in Connection.Table<T>()
                    select i).ToList<T>();

		}

        public int SaveItem<T>(T item) where T : IDatabaseId
		{
            
			if (item.Id != 0)
			{
                Connection.Update(item);
				return item.Id;
			}

			return Connection.Insert(item);

		}
		public int DeleteItem<T>(T item) where T : IDatabaseId, new()
		{

			return Connection.Delete(item);

		}


    }
}
