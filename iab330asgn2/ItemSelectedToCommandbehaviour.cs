﻿using System;
using iab330asgn2.ViewModels;
using Xamarin.Forms;

namespace iab330asgn2
{
    public class ItemSelectedToCommandbehaviour: Behavior<ListView>
    {

       //Method to bind an event to a command to reduce code in View
		protected override void OnAttachedTo(ListView bindable)
		{
			base.OnAttachedTo(bindable);
            bindable.ItemSelected += Bindable_ItemSelected;
		}

      //Gets the item selected and fires the edit command
       async void Bindable_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var lv = sender as ListView;
			var vm = lv.BindingContext as ProductViewModel;
            await vm.EditAsync();
        }
		
        //method to un-bind the command after use
        protected override void OnDetachingFrom(ListView bindable)
		{
			base.OnDetachingFrom(bindable);
			bindable.ItemSelected -= Bindable_ItemSelected;
		}

        public ItemSelectedToCommandbehaviour()
        {
        }
    }
}
