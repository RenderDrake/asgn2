﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using iab330asgn2.Models;
using Xamarin.Forms;
using iab330asgn2.Models.Database;


namespace iab330asgn2.ViewModels
{
    
    public class GroceryViewModel :INotifyPropertyChanged
    {
         /// <summary>
        /// This class is used to separate the grocery data from the grocery view.
        /// It is responsible for utilizing the data in the Grcoery Model and database.
        /// </summary>
        
		private bool isRefreshing = false;
		public bool IsRefreshing
		{
			get { return isRefreshing; }
			set
			{
				isRefreshing = value;
                NotifyPropertyChanged("IsRefreshing");
			}
		}


        private decimal _Total;

        //Gets and Sets the total cost of all groceries
        public decimal Total
        {
            get
            {
                return _Total;
            }
            set
            {
                if (_Total != value)
                {
                    _Total = value;
                    NotifyPropertyChanged("Total");
                }
            }
        }
        public ObservableCollection<Grocery> Groceries { get; set; }

        public ICommand Delete { get; set; }
        public ICommand RefreshGroceries { get; set; }

        //Constructor
        public GroceryViewModel()
        {

            Groceries = new ObservableCollection<Grocery>(App.db.GetItems<Grocery>());

            GetTotal(Groceries);
            Delete = new Command<Grocery>(DeleteItem);//Initialise the delete command
            RefreshGroceries = new Command(RefreshGroceryList);//Initialise the refresh groceries command
        }

        //Method to delete an item in the grocery list
        public void DeleteItem(Grocery item)
        {
            App.db.DeleteItem(item);
         
            Groceries.Clear();//Clear the list
            //Re-populate the list
            foreach(Grocery g in App.db.GetItems<Grocery>()){
                Groceries.Add(g);
            }

            GetTotal(Groceries);//Get the total cost of all groceries
        }

        public bool CanExecute(object parameter)
        {
            throw new NotImplementedException();
        }

        public void Execute(object parameter)
        {
            throw new NotImplementedException();
        }


        //Method to get the total cost of all groceries in the list
        private void GetTotal(ObservableCollection<Grocery> groceries)
        {
            Total = 0;
            if (groceries != null)
            {
                foreach (Grocery item in groceries)
                {
                    Total += item.Cost;
                }
            }else{
                Total = 0;
            }


        }

        //Method to reresh the grocery list if an item is added or deleted
        public void RefreshGroceryList(){
            isRefreshing = true;

			Groceries.Clear();
			foreach (Grocery g in App.db.GetItems<Grocery>())
			{
				Groceries.Add(g);
			}
            GetTotal(Groceries);
            isRefreshing = false;

        }

        //Method to update the View
        #region INPC
        public event PropertyChangedEventHandler PropertyChanged;


        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
       
    }
}


