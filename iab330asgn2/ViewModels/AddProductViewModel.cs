﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;
using iab330asgn2.Models;
using iab330asgn2.Views;
using Xamarin.Forms;

namespace iab330asgn2.ViewModels
{
    public class AddProductViewModel: INotifyPropertyChanged
    {
        /// <summary>
        /// This class provides the methods to add a new product to the products list
        /// </summary>
        
        private string displayName;
        private string aisleNumber;
        private string cost;
        public ICommand AddProduct { get; set; }
        private List<string> types;
        private string typeSelected;


        ////Gets and Sets the selected product from the listview
        public string TypeSelected
        {
            get
            {
                return typeSelected;
            }
            set
            {
                if (typeSelected != value)
                {
                    typeSelected = value;
                    NotifyPropertyChanged("TypeSelected");
                }
            }
        }


        //A list that holds all the product types
        public List<string> Types
        {
            get
            {
                return types;
            }
            set
            {
                if (types != value)
                {
                    types = value;
                    NotifyPropertyChanged("Types");
                }
            }
        }

        //Gets and Sets the product cost for the View
        public string Cost
		{
			get
			{
				return cost;
			}
			set
			{
				if (cost != value)
				{
					cost = value;
					NotifyPropertyChanged("Cost");
				}
			}
		}

        //Gets and Sets the product display name for the View
		public string DisplayName
		{
			get
			{
                return displayName;
			}
			set
			{
                if (displayName != value)
				{
                    displayName = value;
					NotifyPropertyChanged("DisplayName");
				}
			}
		}
        
        //Gets and Sets the product aisle number for the View
		public string AisleNumber
		{
			get
			{
                return aisleNumber;
			}
			set
			{
                if (aisleNumber != value)
				{
                    aisleNumber = value;
					NotifyPropertyChanged("AisleNumber");
				}
			}
		}

        //Constructor
        public AddProductViewModel()
        {
            //Initiate the command to add a product to the products list
            AddProduct = new Command(Add);
            Types = new List<string>{
                "Meats",
                "Fresh Produce",
                "Condiments",
                "Toiletries",
                "Snacks",
                "Dairy",
                "Electrical Goods",
                "Other"
            };
        }



        //Method to notify the View of a change
        #region INPC
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        //Method to add a product to the database
        public async void Add(){
            
            //Checks to see if the productis added succesfully
            if (App.db.SaveItem(new Product
            {
                DisplayName = DisplayName,
                AisleNumber = int.Parse(AisleNumber),
                Cost = decimal.Parse(Cost),
                Type = TypeSelected
            }) > 0)
            {
                //Notify the View and user
                await Application.Current.MainPage.DisplayAlert(displayName, "Was Added!", "OK");
                await Application.Current.MainPage.Navigation.PopAsync(); //Remove the page currently on top.
                await Application.Current.MainPage.Navigation.PushAsync(new MainPage());
            }
        }
    }
}
