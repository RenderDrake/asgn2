﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;
using iab330asgn2.Models;
using iab330asgn2.Views;
using Xamarin.Forms;

namespace iab330asgn2.ViewModels
{
    public class EditProductViewModel: INotifyPropertyChanged
    {
        /// <summary>
        /// This class is is sued to implement the  methods and commands to 
        /// edit products in the product list
        /// </summary>
        
        private string displayName;
        private Product product;
        private string cost;
        private string aisleNumber;
        private string typeSelected;
        private List<string> types;
        public ICommand EditProduct { get; set; }
        public ICommand DeleteProduct { get; set; }
        private int typeindex;


        //Gets or Sets the product cost for the View
        public string Cost
		{
			get
			{
				return cost;
			}
			set
			{
				if (cost != value)
				{
					cost = value;
					NotifyPropertyChanged("Cost");
				}
			}
		}
        
        //Gets or Sets the product index of the type for the View
        public int TypeIndex
        {
            get
            {
                return typeindex;
            }
            set
            {
                if (typeindex != value)
                {
                    typeindex = value;
                    NotifyPropertyChanged("TypeIndex");
                }
            }
        }

        //Gets or Sets the selected type of product
        public string TypeSelected
        {
            get
            {
                return typeSelected;
            }
            set
            {
                if (typeSelected != value)
                {
                    typeSelected = value;
                    NotifyPropertyChanged("TypeSelected");
                }
            }
        }

        //Lsit of available types
        public List<string> Types
        {
            get
            {
                return types;
            }
            set
            {
                if (types != value)
                {
                    types = value;
                    NotifyPropertyChanged("Types");
                }
            }
        }

        //Gets or Sets the product display name for the View
		public string DisplayName
		{
			get
			{
				return displayName;
			}
			set
			{
				if (displayName != value)
				{
					displayName = value;
					NotifyPropertyChanged("DisplayName");
				}
			}
		}

        //Gets or Sets the product aisle number for the View
		public string AisleNumber
		{
			get
			{
				return aisleNumber;
			}
			set
			{
				if (aisleNumber != value)
				{
					aisleNumber = value;
					NotifyPropertyChanged("AisleNumber");
				}
			}
		}

        //Constructor
        public EditProductViewModel(Product p)
        {
            product = p;
            displayName = product.DisplayName;
            aisleNumber = product.AisleNumber.ToString();
            cost = product.Cost.ToString();
            Types = new List<string>{
                "Meats",
                "Fresh Produce",
                "Condiments",
                "Toiletries",
                "Snacks",
                "Dairy",
                "Electrical Goods",
                "Other"
            };
            
            //Display the current type each product is
            if (product.Type != null)
            {
                typeindex = Types.IndexOf(product.Type);
            }else{
                typeindex = 1;
            }

            EditProduct = new Command(Edit);//Initialise the edit product command
            DeleteProduct = new Command(Delete);//Initialise the delete product command
           

        }

        //Methdo to delete the selected product
        public async void Delete()
        {
            App.db.DeleteItem(product);
            await Application.Current.MainPage.DisplayAlert(product.DisplayName, "Was Deleted", "OK");
            await Application.Current.MainPage.Navigation.PopAsync(); //Remove the page currently on top.
            await Application.Current.MainPage.Navigation.PushAsync(new MainPage());

        }

        //Method to edit the selected product
        public async void Edit(){
            Product editProduct = new Product();
            //Find the selected product in the database
            foreach(Product p in App.db.GetItems<Product>()){
                if(p.Id == product.Id){
                    editProduct = p;
                }
            }

            //App.db.DeleteItem(editProduct);
            
            //Update each value of the product
            editProduct.DisplayName = DisplayName;
            editProduct.AisleNumber = int.Parse(AisleNumber);
            editProduct.Cost = decimal.Parse(Cost);
            editProduct.Type = TypeSelected;


            //Try and and save the product
            if (App.db.SaveItem(editProduct) > 0)
            {
                //If succesfully saved, notify the user
                await Application.Current.MainPage.DisplayAlert(editProduct.DisplayName, "Was Edited", "OK");
                await Application.Current.MainPage.Navigation.PopAsync(); //Remove the page currently on top.
                await Application.Current.MainPage.Navigation.PushAsync(new MainPage());
            }

        }

        //Method to notify the View of a change
        #region INPC
        public event PropertyChangedEventHandler PropertyChanged;
        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
