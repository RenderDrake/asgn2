﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using iab330asgn2.Models;
using iab330asgn2.Views;
using Xamarin.Forms;


namespace iab330asgn2.ViewModels
{
    public class ProductViewModel:INotifyPropertyChanged
    {
        /// <summary>
        /// This class is used to separate the product data from the product view.
        /// It is responsible for utilizing the data in the Product Model and database.
        /// </summary>
        
        private string searchText;
        public ObservableCollection<Product> Products { get; set; }
        public ICommand SearchProducts { get; set; }
		public ICommand Add { get; set; }
        public TextChangedEventArgs TextChanged;
        public ICommand Export { get; set; }
        public event PropertyChangedEventHandler PropertyChanged;
		public ICommand RefreshProducts { get; set; }
        private Product selectedProduct;
		private bool isRefreshing = false;
        private populate_products pp;
        private ObservableCollection<Product> testEmpty;
        private List<String> pickerList;


        //Gets and Sets the text entered in the search bar
        public string SearchText { get
            {
                return searchText;
            }

            set
            {
                searchText = value;
                NotifyPropertyChanged("SearchText");

            } }

        
        public List<String> Picker{
            get
            {
                return pickerList;
            }

            set
            {
                pickerList = value;
                NotifyPropertyChanged("Picker");

            }
        }

        //Gets and Sets the selected product
        public Product SelectedProduct
		{
			get
			{
                return selectedProduct;
			}

			set
			{
                selectedProduct = value;
                NotifyPropertyChanged("SelectedItem");

			}
		}


        //Boolean value to see if the product list is refreshing
		public bool IsRefreshing
		{
			get { return isRefreshing; }
			set
			{
				isRefreshing = value;
				NotifyPropertyChanged("IsRefreshing");
			}
		}

        //Constructor
        public ProductViewModel()
        {

            //Check if the product database is empty
            testEmpty = new ObservableCollection<Product>();
            foreach(Product p in App.db.GetItems<Product>()){
                testEmpty.Add(p);
            }

            //If the databas is empty populate it with the default text file
            if(testEmpty.Count == 0){
                pp = new populate_products(App.db);
            }

            Products = new ObservableCollection<Product>(App.db.GetItems<Product>());
            Picker = new List<String>{
                "Name",
                "Aisle",
                "Price"
            };
            Add = new Command<Product>(AddItem);//Initialise the add command
            SearchProducts = new Command(Search);//Initialise the search products command
            RefreshProducts = new Command(RefreshProductList);//Initialise the refresh products command
            Export = new Command(ExportFile);//Initialise the export command
        }
        
        //Method to add an item to the grocery list
        #region AddItem
        public void AddItem(Product item)
        {

            Grocery gl = null;
            Grocery gs = new Grocery();
            //Check if the grocery exists
            foreach (Grocery g in App.db.GetItems<Grocery>())
            {
                if (g.DisplayName.Equals(item.DisplayName))
                {
                    gl = g;
                }
            }

            //If there is an existing item, update with new values
            if (gl != null)
            {
                gs.DisplayName = gl.DisplayName;
                gs.Amount = 1 + gl.Amount;
                gs.Cost = gl.Cost + gl.BaseCost;
                gs.AisleNumber = gl.AisleNumber;
                gs.BaseCost = gl.BaseCost;
            }
            //Add a new item
            else
            {
                gs.DisplayName = item.DisplayName;
                gs.Amount = 1;
                gs.Cost = item.Cost;
                gs.BaseCost = item.Cost;
                gs.AisleNumber = item.AisleNumber;
            }
            
            //Delete the any current instance of that item
            foreach (Grocery g in App.db.GetItems<Grocery>())
            {
                if (g.DisplayName.Equals(item.DisplayName))
                {
                    App.db.DeleteItem(g);
                }
            }
            
            //Add the new item
            App.db.SaveItem(gs);
        }
        #endregion



        //Method to search through the list of products
        #region Search
        public void Search()
        {

            Products.Clear();
            foreach (Product p in App.db.GetItems<Product>())
            {
                Products.Add(p);
            }
            String keyword = SearchText.ToLower();
            ObservableCollection<Product> productsRefined = new ObservableCollection<Product>();

            //Match the keyword with the product name, aisle and cost
            foreach (Product i in Products)
            {
                String newCost = i.Cost.ToString();
                String newAisle = i.AisleNumber.ToString();
                if (i.DisplayName.ToLower().Contains(keyword) || newCost.Contains(keyword) || newAisle.Contains(keyword) )
                {
                    productsRefined.Add(i);
                }
            }

            //Populate the products list with matched values
            Products.Clear();
            foreach (Product p in productsRefined)
            {
                Products.Add(p);
            }
        }
        #endregion

        //Method to refresh the products list display when an item is added or deleted
        #region RefreshProductList
        public void RefreshProductList()
        {
            isRefreshing = true;
            SearchText = "";
            Products.Clear();
            foreach (Product g in App.db.GetItems<Product>())
            {
                Products.Add(g);
            }

            isRefreshing = false;

        }
		#endregion

        //Method to update the View
		#region INPC
		private void NotifyPropertyChanged(string propertyName)
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
		}
        #endregion

        //Method to export the product list
        public void ExportFile(){
            DependencyService.Get<ISaveAndLoad>().SaveText("Test.txt", Products);
        }

        //Load a new edit product page
        public async System.Threading.Tasks.Task EditAsync()
        {
            
            await Application.Current.MainPage.Navigation.PushAsync(new EditProductPage(SelectedProduct));
        }
    }
}
