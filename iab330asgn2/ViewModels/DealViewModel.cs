﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Windows.Input;
using System.Xml.Linq;
using iab330asgn2.Models;
using Xamarin.Forms;

namespace iab330asgn2.ViewModels
{
    public class DealViewModel: INotifyPropertyChanged
    {
        /// <summary>
        /// This class provides the information for the deals view. 
        /// It helps sepearte the data about the deals from the display.
        /// </summary>
        
        private string searchText;
        public ICommand SearchProducts { get; set; }
        public ICommand Add { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<Deal> Deals { get; set; }

        //Gets and Sets the tetx entered in the search bar in the View
        public string SearchText
        {
            get
            {
                return searchText;
            }

            set
            {
                searchText = value;
                NotifyPropertyChanged("SearchText");

            }
        }

        public DealViewModel()
        {
            #region Check Empty Deal
            List<Deal> d = new List<Deal>(App.db.GetItems<Deal>()); //New deal list
            if (d.Count == 0)// Add default deals if the deals list is empty
            {
                App.db.SaveItem(new Deal
                {
                    DisplayName = "Barilla Pasta Sauce 400g",
                    AisleNumber = 4,
                    Cost = 1.87M,
                    URL = "https://d2g5na3xotdfpc.cloudfront.net/images/thumbs/ipad/189271067.jpg?ts=1507682467"

                });

                App.db.SaveItem(new Deal
                {
                    DisplayName = "Golden Circle Australian Pineapple 440-450g",
                    AisleNumber = 2,
                    Cost = 2.70M,
                    URL = "https://d2g5na3xotdfpc.cloudfront.net/images/thumbs/ipad/189271078.jpg?ts=1507682696"
                });

                App.db.SaveItem(new Deal
                {
                    DisplayName = "Green’s Pancake Shake 325-375g or Green’s Traditional Baking Mixes 340-470g – Excludes Gluten Free",
                    AisleNumber = 5,
                    Cost = 2.00M,
                    URL = "https://d2g5na3xotdfpc.cloudfront.net/images/thumbs/ipad/189271077.jpg?ts=1507682684"
                });


                App.db.SaveItem(new Deal
                {
                    DisplayName = "Golden Canola Oil 4 Litre",
                    AisleNumber = 3,
                    Cost = 9.00M,
                    URL = "https://d2g5na3xotdfpc.cloudfront.net/images/thumbs/ipad/189271070.jpg?ts=1508193974"
                });


                App.db.SaveItem(new Deal
                {
                    DisplayName = "Nestle Milo Cereal 700g or Nestle Nesquik Cereal 650g",
                    AisleNumber = 6,
                    Cost = 3.49M,
                    URL = "https://d2g5na3xotdfpc.cloudfront.net/images/thumbs/ipad/189271060.jpg?ts=1507678263"
                });




            }

            #endregion
            SearchProducts = new Command(Search);//Initilaise the search products command
            Add = new Command<Deal>(AddItem);//Initilaise the add command
            Deals = new ObservableCollection<Deal>(App.db.GetItems<Deal>());//Itemsource for the deals view
        }


        //Method to add a deal to the database
        #region AddItem
        public void AddItem(Deal item)
        {
            
            Grocery gl = null;
            Grocery gs = new Grocery();
            //Check if the deal exists in the grocery database
            foreach (Grocery g in App.db.GetItems<Grocery>())
            {
                if (g.DisplayName.Equals(item.DisplayName))
                {
                    gl = g;
                }
            }

            //If the item does exist update the entry
            if (gl != null)
            {
                gs.DisplayName = gl.DisplayName;
                gs.Amount = 1 + gl.Amount;
                gs.Cost = gl.Cost + gl.BaseCost;
                gs.AisleNumber = gl.AisleNumber;
                gs.BaseCost = gl.BaseCost;
            }
            
            //If not make a new entry
            else
            {
                gs.DisplayName = item.DisplayName;
                gs.Amount = 1;
                gs.Cost = item.Cost;
                gs.BaseCost = item.Cost;
                gs.AisleNumber = item.AisleNumber;
            }

            //Delete any existing items
            foreach (Grocery g in App.db.GetItems<Grocery>())
            {
                if (g.DisplayName.Equals(item.DisplayName))
                {
                    App.db.DeleteItem(g);
                }
            }
            
            //Add the new item
            App.db.SaveItem(gs);
        }
        #endregion




















        #region Search
        public void Search()
        {

            Deals.Clear();
            foreach (Deal d in App.db.GetItems<Deal>())
            {
                Deals.Add(d);
            }
            String keyword = SearchText.ToLower();
            ObservableCollection<Deal> dealsRefined = new ObservableCollection<Deal>();


            foreach (Deal i in Deals)
            {
                String newCost = i.Cost.ToString();
                String newAisle = i.AisleNumber.ToString();
                if (i.DisplayName.ToLower().Contains(keyword) || newCost.Contains(keyword) || newAisle.Contains(keyword))
                {
                    dealsRefined.Add(i);
                }
            }

            Deals.Clear();
            foreach (Deal p in dealsRefined)
            {
                Deals.Add(p);
            }



        }
        #endregion






        private void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }


    }
}
