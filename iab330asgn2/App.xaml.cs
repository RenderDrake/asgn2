﻿using iab330asgn2.Views;
using System;
using iab330asgn2;
using iab330asgn2.Models.Database;
using Xamarin.Forms;

namespace iab330asgn2

{
	public partial class App : Application
	{
        public static Database db;
		public App()
		{
            db = new Database();
			InitializeComponent();

			MainPage = new NavigationPage(new MainPage());

		}

		protected override void OnStart()
		{
			// Handle when your app starts
		}

		protected override void OnSleep()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume()
		{
			// Handle when your app resumes
		}
	}
}
