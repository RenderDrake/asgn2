﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using iab330asgn2.Models;

namespace iab330asgn2
{
    //Interface for saving and loading text files
    public interface ISaveAndLoad
    {
		void SaveText(string filename, ObservableCollection<Product> p);
		string LoadText(string filename);
    }
}
